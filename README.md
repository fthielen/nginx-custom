# Custom Nginx install
***
Dieses Script dient dazu, Nginx mit folgenden Modulen automatisch zu kompilieren:
* [SSL](http://nginx.org/en/docs/http/ngx_http_ssl_module.html)
* [Stream](https://nginx.org/en/docs/stream/ngx_stream_core_module.html)
* [Mail](http://nginx.org/en/docs/mail/ngx_mail_core_module.html)
* [HTTP v2](https://nginx.org/en/docs/http/ngx_http_v2_module.html)
* [RealIP](http://nginx.org/en/docs/http/ngx_http_realip_module.html)
* [WebDAV](http://nginx.org/en/docs/http/ngx_http_dav_module.html)
* [Gunzip](http://nginx.org/en/docs/http/ngx_http_gunzip_module.html)
* [Gzip Static](http://nginx.org/en/docs/http/ngx_http_gzip_static_module.html)
* [HTTP Slice](https://nginx.org/en/docs/http/ngx_http_slice_module.html)
* [Stub Status](https://nginx.org/en/docs/http/ngx_http_stub_status_module.html)
* [HTTP Addition](http://nginx.org/en/docs/http/ngx_http_addition_module.html)
* [Naxsi](https://github.com/nbs-system/naxsi)
* [Fancy Index](https://github.com/aperezdc/ngx-fancyindex)
* [Headers More](https://github.com/openresty/headers-more-nginx-module)
* [Pagespeed](https://github.com/pagespeed/ngx_pagespeed)

***
## Installation:
1. **Repository herunterladen:** ```git clone https://gitlab.com/fthielen/nginx-custom.git```
2. **In das Verzeichnis wechseln:** ```cd nginx-custom```
3. **Script ausführen:** ```./install.sh```

> **Hinweis:**  
> Wenn eine Meldung wie "git command not found" kommt, muss git noch nach installiert werden:  
> *Debian/Ubuntu:* ```apt-get install git```  
> *CentOS/Fedora:* ```yum install git```  
> *Suse/OpenSUSE:* ```zypper install git```  