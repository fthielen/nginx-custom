#!/bin/bash

set -e

if [[ -d /usr/local/nginx ]]; then
    rm -rf /usr/local/nginx
fi

if [[ -f /etc/systemd/system/nginx.service ]]; then
    rm /etc/systemd/system/nginx.service
fi

lowercase(){
    echo "$1" | sed "y/ABCDEFGHIJKLMNOPQRSTUVWXYZ/abcdefghijklmnopqrstuvwxyz/"
}

OS=`lowercase \`uname\``

if [ -f /etc/os-release ]; then
    # freedesktop.org and systemd
    . /etc/os-release
    OS=$ID_LIKE
    ID=$ID
    VER=$VERSION_ID
elif type lsb_release >/dev/null 2>&1; then
    # linuxbase.org
    OS=$(lsb_release -si)
    VER=$(lsb_release -sr)
elif [ -f /etc/lsb-release ]; then
    # For some versions of Debian/Ubuntu without lsb_release command
    . /etc/lsb-release
    OS=$DISTRIB_ID
    VER=$DISTRIB_RELEASE
elif [ -f /etc/debian_version ]; then
    # Older Debian/Ubuntu/etc.
    OS=Debian
    VER=$(cat /etc/debian_version)
else
    # Fall back to uname, e.g. "Linux <version>", also works for BSD, etc.
    OS=$(uname -s)
    VER=$(uname -r)
fi

read -p 'Soll Nginx mit Lets Encrypt eingerichtet werden? (y/n)' ifle

if [ "$ifle" == "Y" ] || [ "$ifle" == "y" ]; then
	git clone https://github.com/certbot/certbot.git /opt/certbot

	if [ "$OS" == "fedora" ] || [ "$ID" == "fedora" ] || [ "$OS" == "centos" ] || [ "$ID" == "centos" ]; then
		ln -s /opt/certbot/certbot-auto /usr/local/sbin/certbot
		ln -s /opt/certbot/certbot-auto /usr/local/sbin/letsencrypt
		cp make_cert /usr/local/sbin
		chmod +x /usr/local/sbin/make_cert
	else
		ln -s /opt/certbot/certbot-auto /usr/local/bin/certbot
		ln -s /opt/certbot/certbot-auto /usr/local/bin/letsencrypt
		cp make_cert /usr/local/bin
		chmod +x /usr/local/bin/make_cert
	fi
	crontab -l | { cat; echo "@weekly certbot renew --pre-hook 'systemctl stop nginx' --post-hook 'systemctl start nginx' --quiet"; } | crontab -
fi

if [ "$OS" == "fedora" ] || [ "$ID" == "fedora" ]; then
	dnf install gcc gcc-c++ autoconf automake make perl -y
elif [ "$OS" == "debian" ] || [ "$ID" == "ubuntu" ] || [ "$ID" == "debian" ]; then
	apt-get install gcc g++ autoconf automake make perl openssl libssl-dev -y
elif [ "$OS" == "centos" ] || [ "$ID" == "centos" ]; then
	yum install gcc gcc-c++ autoconf automake make perl wget -y
elif [ "$OS" == "suse" ] || [ "$ID" == "opensuse" ]; then
	zypper install -y gcc gcc-c++ autoconf automake make perl
fi

wget ftp://ftp.csx.cam.ac.uk/pub/software/programming/pcre/pcre-8.41.tar.gz
tar -zxf pcre-8.41.tar.gz
cd pcre-8.41
./configure
make
make install
cd ..
cd zlib-1.2.11
./configure
make
make install
cd ..
cd openssl-1.0.2f
./Configure linux-x86_64 --prefix=/usr
make
make install
cd ..
cd ngx_pagespeed-latest-beta
wget https://dl.google.com/dl/page-speed/psol/1.12.34.2-x64.tar.gz
tar -xzvf 1.12.34.2-x64.tar.gz
cd ..

./configure --sbin-path=/usr/local/nginx/nginx --conf-path=/usr/local/nginx/nginx.conf --pid-path=/usr/local/nginx/nginx.pid --with-pcre=pcre-8.41 --with-zlib=zlib-1.2.11 --with-http_ssl_module --with-stream --with-mail=dynamic --with-ipv6 --with-http_v2_module --with-http_realip_module --with-http_dav_module --with-http_gunzip_module --with-http_gzip_static_module --with-http_slice_module --with-http_stub_status_module --with-http_addition_module --add-module=naxsi-0.55.3 --add-module=ngx-fancyindex --add-dynamic-module=headers-more-nginx-module-0.32 --add-module=ngx_pagespeed-latest-beta
make
make install

rm -rf /usr/local/nginx/nginx.conf
cp nginx.conf /usr/local/nginx/
mkdir -p /usr/local/nginx/snippets
cp ssl-params.conf /usr/local/nginx/snippets/

if [ "$OS" == "fedora" ] || [ "$ID" == "fedora" ] || [ "$OS" == "centos" ] || [ "$ID" == "centos" ]; then
	cp ngensite /usr/local/sbin
	chmod +x /usr/local/sbin/ngensite
else
	cp ngensite /usr/local/bin
	chmod +x /usr/local/bin/ngensite
fi

mkdir -p /var/log/nginx/

mv nginx.service /etc/systemd/system/
systemctl daemon-reload
systemctl enable nginx

git clone https://github.com/h5bp/server-configs-nginx.git /tmp/server-configs-nginx
mv /tmp/server-configs-nginx/h5bp /usr/local/nginx/
rm -rf /tmp/server-configs-nginx
openssl dhparam -out /etc/ssl/certs/dhparam.pem 2048
cd /usr/local/nginx
mkdir sites-available
mkdir sites-enabled

if [ "$OS" == "fedora" ] || [ "$ID" == "fedora" ] || [ "$OS" == "centos" ] || [ "$ID" == "centos" ]; then
	ln -s /usr/local/nginx/nginx /usr/local/sbin/nginx
else
	ln -s /usr/local/nginx/nginx /usr/local/bin/nginx
fi

echo ""
echo "----------------"
echo "Installation abgeschlossen!"
echo "Du kannst Nginx jetzt mit systemctl start nginx starten!"
echo "Deine vHosts kannst du im Verzeichnis /usr/local/nginx/sites-available ablegen, und dann mit dem Befehl ngensite datei aktivieren!"